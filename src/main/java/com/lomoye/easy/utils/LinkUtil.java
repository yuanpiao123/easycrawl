package com.lomoye.easy.utils;

import org.assertj.core.util.Strings;

/**
 * 2020/11/9 13:45
 * yechangjun
 * 链接合法转化工具
 */
public class LinkUtil {

    /**
     * 获取域名
     * @param url 链接
     * @return 域名
     */
    public static String getDomain(String url) {
        if (Strings.isNullOrEmpty(url)) {
            return url;
        }
        String domain;
        if (url.contains("//")) {
            domain = url.substring(url.indexOf("//") + 2);
        } else {
            domain = url;
        }

        if (!domain.contains("/")) {
            return domain;
        }

        return domain.substring(0, domain.indexOf("/"));
    }

    public static String merge(String url, String domain) {
        if (url.startsWith("//")) {
            url = "http:" + url;
        } else if (url.startsWith("/")) {
            url = "http://" + domain + url;
        }
        return url;
    }
}
